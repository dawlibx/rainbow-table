require 'openssl'

# Maximum number of iterations for password searching method
# it's multiplication of iterations number per element and elements number
MAX_ITERATIONS_NR = 10
NOT_FOUND = 'Sorry, password was not found'.freeze

# @example
#   => "5703"
def run_example
  rainbow_table = []

  rainbow_table << generate_rainbow_table_record('7789', 2)
  rainbow_table << generate_rainbow_table_record('3253', 6)
  rainbow_table << generate_rainbow_table_record('9113', 5)

  search_in_rainbow_table('f7dd39d47c6f28f7877155ccffad0192', rainbow_table)
end

# Simple reduction function,
# it takes first four digits of a given string type parameter named hash
# @param hash [String]
# @return [String] reduced hash
def reduce_hash(hash)
  hash.gsub(/[^0-9]+/, '')[0..3]
end

# Rainbow table record generator
# @param password [String] password for which we want to generate record for
#   rainbow table
# @param i [Integer] number of iterations
# @return [Hash] key-value object with password, hash and number of iternations
def generate_rainbow_table_record(password, i)
  hash = OpenSSL::Digest::MD5.new(password).to_s

  (i - 1).times { hash = OpenSSL::Digest::MD5.new(reduce_hash(hash)).to_s }

  { password: password, hash: hash, i: i }
end

# It searches for password in rainbow table
# @param input_hash [String]
# @param rainbow_table [Array]
# @return [String] password if found
def search_in_rainbow_table(input_hash, rainbow_table)
  hash = input_hash

  MAX_ITERATIONS_NR.times do
    rainbow_table.each do |record|
      return find_password(input_hash, record) if record[:hash] == hash
    end

    hash = OpenSSL::Digest::MD5.new(reduce_hash(hash)).to_s
  end

  NOT_FOUND
end

# Performs calculations to find password
# @param input_hash [String]
# @param record [Hash] record of rainbow table
# @return [String] password if found
def find_password(input_hash, record)
  hash = OpenSSL::Digest::MD5.new(record[:password]).to_s

  (record[:i] - 1).times do
    searched_password = reduce_hash(hash)
    hash = OpenSSL::Digest::MD5.new(searched_password).to_s

    return searched_password if input_hash == hash
  end

  NOT_FOUND
end
